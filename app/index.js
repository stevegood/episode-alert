import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import routes from './config/routes';
import './style.css';

injectTapEventPlugin();

ReactDOM.render(routes, document.getElementById('app'));
