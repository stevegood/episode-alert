import React from 'react';
import Login from '../components/Login';

class LoginContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      appId: '859983064127863'
    };

    this.handleFacebookLogin = this.handleFacebookLogin.bind(this);
  }

  handleFacebookLogin(facebookUser) {
    // TODO: dispatch an action that the user has logged in
    this.context.router.push({
      pathname: '/search'
    });
  }

  render() {
    return (
      <Login
        appId={this.state.appId}
        onFacebookLogin={this.handleFacebookLogin} />
    );
  }
}

LoginContainer.contextTypes = {
  router: React.PropTypes.object.isRequired
};

export default LoginContainer;
