import appBar from './appBar';
import page from './page';

const styles = {
  appBar,
  page
}

export default styles;
