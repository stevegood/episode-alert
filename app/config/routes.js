import React from 'react';
import {
  Route,
  Router,
  IndexRoute,
  hashHistory
} from 'react-router';

import Home from '../components/Home';
import LoginContainer from '../containers/LoginContainer';
import Main from '../components/Main';
import Search from '../components/Search';

const routes = (
  <Router history={hashHistory}>
    <Route path='/' component={Main}>
      <IndexRoute component={Home} />
      <Route path='login' component={LoginContainer} />
      <Route path='search' component={Search} />
    </Route>
  </Router>
);

export default routes;
