import React from 'react';
import styles from '../styles';
import {AppBar, LeftNav, MenuItem} from 'material-ui';

const Header = (props) => (
  <div>
    <AppBar
      title={props.title}
      titleStyle={styles.appBar.title}
      onTitleTouchTap={props.onTitleClick}
      onLeftIconButtonTouchTap={props.onNavButtonClick} />

    <LeftNav
      docked={false}
      open={props.open}
      onRequestChange={props.onRequestChange}>

      <MenuItem onTouchTap={props.onLoginMenuItemClick}>
        Login / Register
      </MenuItem>

    </LeftNav>
  </div>
);

export default Header;
