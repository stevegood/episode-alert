import React from 'react';
import {Col, Row} from 'react-flexbox-grid';
import {FlatButton, TextField} from 'material-ui';

export default class Search extends React.Component {
  render() {
    return (
      <Row>
        <Col xs={6}>
          <TextField floatingLabelText="Find a show" />
          <FlatButton label="Search" primary={true} />
        </Col>
      </Row>
    )
  }
}
