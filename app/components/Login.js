import React from 'react';
import {Col, Row} from 'react-flexbox-grid';
import FacebookLogin from 'react-facebook-login';

const Login = (props) => (
  <Row center='xs'>
    <Col xs={8}>
      <FacebookLogin
        appId={props.appId}
        autoLoad={false}
        callback={props.onFacebookLogin} />
    </Col>
  </Row>
);

export default Login;
