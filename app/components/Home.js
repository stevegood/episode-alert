import React from 'react';
import {Link} from 'react-router';
import {Col, Row} from 'react-flexbox-grid';

const Home = (props) => (
  <div>
    <Row>
      <Col xs={12}>
        <h1>Home</h1>
      </Col>
    </Row>

    <Row>
      <Col xs={12}>
        <Link to='/login'>Login / Register</Link>
      </Col>
    </Row>
  </div>
);

export default Home;
