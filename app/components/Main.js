import React from 'react';
import styles from '../styles';
import {Col, Grid, Row} from 'react-flexbox-grid';
import {MenuItem} from 'material-ui';
import Header from './Header';

class Main extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
      leftNavOpen: false
    }

    this.closeLeftNav = this.closeLeftNav.bind(this);
    this.openLeftNav = this.openLeftNav.bind(this);
    this.handleTitleClick = this.handleTitleClick.bind(this);
    this.handleNavButtonClick = this.handleNavButtonClick.bind(this);
    this.handleLoginMenuItemClick = this.handleLoginMenuItemClick.bind(this);
  }

  closeLeftNav() {
    this.setState({leftNavOpen: false});
  }

  openLeftNav() {
    this.setState({leftNavOpen: true});
  }

  handleTitleClick() {
    this.context.router.push({
      pathname: '/'
    });
  }

  handleNavButtonClick() {
    this.openLeftNav();
  }

  handleLoginMenuItemClick() {
    this.context.router.push({
      pathname: '/login'
    });
    this.closeLeftNav();
  }

  render() {
    return (
      <div>
        <Header
          title='Episode Alert'
          onTitleClick={this.handleTitleClick}
          onNavButtonClick={this.handleNavButtonClick}
          open={this.state.leftNavOpen}
          onRequestChange={leftNavOpen => this.setState({leftNavOpen})}
          onLoginMenuItemClick={this.handleLoginMenuItemClick} />

        <Grid fluid>
          <Row style={styles.page.inner}>
            <Col xs={12}>
              {this.props.children}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

Main.contextTypes = {
  router: React.PropTypes.object.isRequired
};

export default Main;
