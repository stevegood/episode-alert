var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config.js');
var port = process.env.PORT || 8080;
var compiler = webpack(config);

var server = new WebpackDevServer(compiler, {
  stats: { colors: true }
});

server.listen(port, '0.0.0.0', function(){});
